from django.contrib import admin
from .models import *


class PostAdmin(admin.ModelAdmin):
    list_filter = ('title', 'created','updated')
    list_display = ('title', 'created','updated','status')
    search_fields = ('title', 'content')


class CommentAdmin(admin.ModelAdmin):
    list_display = ('post', 'author', 'text', 'timestamp')
    list_filter = ('post', 'author')
    search_fields = ('text', 'author', 'post')


admin.site.register(Post, PostAdmin)
admin.site.register(Comment, CommentAdmin)