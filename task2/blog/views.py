from django.shortcuts import render, get_object_or_404, redirect
from .models import *
from .forms import *
from django.contrib.auth import login, authenticate
from django.views.generic import RedirectView
from django.db.models import Q
from django.contrib.auth.models import User
from django.utils import timezone


def index(request):
    template = 'posts/post-list.html'
    posts = Post.objects.filter(status='Published').order_by("-created")
    content = {
        'posts': posts
    }
    return render(request, template, content)


def post_detail(request, pk):
    template = 'posts/post-detail.html'
    post = get_object_or_404(Post, pk=pk)
    comments = Comment.objects.filter(post=post)
    if request.method == "POST":
        form = Comment(request.POST or None)
        if form.is_valid():
            content = request.POST.get('content')
            comment = Comment.objects.create(post=post, user=request.user, content=content)
            comment.save()
            return redirect(request.META['HTTP_REFERER'])
    else:
        form = CommentForm()
    content = {
        'post': post,
        'comments': comments,
        'form': form
    }
    return render(request, template, content)


def post_new(request):
    template = ''
    if request.method == 'POST':
        form = PostForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
        return redirect('blog:post-list')
    else:
        form = PostForm()
    content = {
        'form': form
    }
    return request(request, template, content)


def post_edit(request, pk):
    template = ''
    post = get_object_or_404(Post, pk=pk)
    if request.method == 'POST':
        form = PostForm(request.POST, request.FILES,instance=post)
        if form.is_valid():
            form.save()
        return redirect('blog:post-list')
    else:
        form = PostForm(instance=post)

    content = {
        'form': form
    }
    return request(request, template, content)


def search(request):
    template = 'posts/post-list.html'
    query = request.GET.get('q')
    if query:
        result = Post.objects.filter(Q(title__icontains=query) | Q(content__icontains=query)).order_by("-created")
    else:
        result = Post.objects.filter(status='Published').order_by("-created")

    content = {
        'posts': result
    }
    return render(request, template, content)


class PostLikeToggle(RedirectView):
    def get_redirect_url(self, *args, **kwargs):
        pk = self.kwargs.get("pk")
        print(pk)
        obj = get_object_or_404(Post,pk=pk)
        url_ = obj.get_absolute_url()
        user = self.request.user
        if user.is_authenticated:
            if user in obj.likes.all():
                obj.likes.remove(user)
            else:
                obj.likes.add(user)
        return url_


def signup(request):
    template = 'registration/registration.html'
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect('blog:blog_list_view')
    else:
        form = SignUpForm()
    return render(request, template, {'form': form})


def delete_comment(request, pk):
    comment = Comment.objects.get(pk=pk)
    if comment.author == request.user:
        comment.is_removed = True
        comment.save()
        comment.delete()
    return redirect(request.META['HTTP_REFERER'])