
from django.contrib import admin
from django.urls import path, include
from blog.views import *

app_name = 'blog'

urlpatterns = [
    path('home/', index, name='post-list'),
    path('post/<int:pk>/', post_detail,name='post-detail'),
    path('signup/', signup, name='signup'),
    path('(?P<pk>[-\w]+)/delete', delete_comment, name='delete-comment')
]
